#!/usr/bin/env bash

find_shortest-path () {
    # search for shortest path to file/dir $1 by successively cd'ing to parent
    # dir. until result is found or maximum depth $2 (default=2) is reached
    local -a results
    for ((d = 0; d < ${2:-2}; d++)); do
        cd ..
        mapfile -t results < <(find . -name "$1")
        if [[ -n "${results[*]}" ]]; then
            # TODO: replace sort by something smarter
            final="$(printf "%s\n" "${results[@]}" | awk 'NF{print $0,NF-1}' | \
                sort | awk '{print $1; exit}')"
            [[ -n "$final" ]] && break
        fi
    done
    realpath "$final"
}

if [[ "$0" == "${BASH_SOURCE[0]}" ]]; then
    find_shortest-path "$@"
fi
