#!/usr/bin/env bash
# ^ currently recommended she-bang for user scripts

# access script line number
echo "hello from line $LINENO"

# bash-ism to determined whether script is sourced or executed
if [[ "$0" == "${BASH_SOURCE[0]}" ]]; then
    echo executed
else
    echo sourced
fi
