#!/usr/bin/env bash

function when_executed () {
    echo "executed: calling ${FUNCNAME[0]}"
}

when_sourced () {
    echo "source with stack ${BASH_SOURCE[@]}"
}

[[ "$0" = "${BASH_SOURCE[0]}" ]] && when_executed || when_sourced
