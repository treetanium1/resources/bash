function parse_options () {
    # reset
    unset CONFIG OUTPUT SUFFIX VERBOSE DRY

    # transform long options to short ones as getopts doesn't support long opt.s, see
    # https://stackoverflow.com/questions/12022592/how-can-i-use-long-options-with-the-bash-getopts-builtin
    for arg in "$@"; do
        shift
        case "$arg" in
            --help)    set -- "$@" '-h'   ;;
            --config)  set -- "$@" '-c'   ;;
            --output)  set -- "$@" '-o'   ;;
            --ext)     set -- "$@" '-e'   ;;
            --verbose) set -- "$@" '-v'   ;;
            --dry)     set -- "$@" '-n'   ;;
            *)         set -- "$@" "$arg" ;;
        esac
    done

    OPTIND=1 # reset for getopts to work
    while getopts ":hc:o:e:vn-" opt; do
        case "$opt" in
            h) print_help; return;;
            c) CONFIG="$OPTARG";;
            o) OUTPUT="$OPTARG";;
            e) SUFFIX="$OPTARG";;
            v) VERBOSE=true;;
            n) DRY=true;;
            -) break;;
            :) echo "Option requires an argument: -${OPTARG}"; return 1;;
            ?) echo "Invalid option: -${OPTARG}"; return 1;;
        esac
    done
}

print_help () {
    cat << EOF
Usage: ${FUNCNAME[1]} [options]... [arguments]...
Options:
    -h,--help     print this help
    -o,--output   output file
    -e,--ext      output file extension (suffix)
    -c,--config   config file
    -v,--verbose  verbose output
    -n,--dry      dry run, no actual changes
    --            interpret arguments after not as options
EOF
}
