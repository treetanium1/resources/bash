#!/usr/bin/env bash

# exit 1 if called by shell, return 1 if called from function
die () {
    [[ -n "$*" ]] && echo "$*" >&2
    # if previous function in stack is nothing or 'main', this function has been called
    # from outside a function, so from a script or a shell
    [[ -z "${FUNCNAME[1]}" || "${FUNCNAME[1]}" == 'main' ]] && exit 1 || return 1
}

anotherday () {
    echo "try again!"
}

_test () {
    nonsense="$(ls monkey)" || die "no time for no monkey business!"
}

if [[ "$0" == "${BASH_SOURCE[0]}" ]]; then
    set -x
    # function: should use return
    _test

    echo '=========='

    # subshell: should use exit
    (
        ls monkey || die "no time for no monkey business!"
    ) || anotherday

    echo '=========='

    # script: should use exit
    ls monkey || die "no time for no monkey business!"
    set +x
fi
