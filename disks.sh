#!/usr/bin/env bash

# === lsblk ===
test_lsblk() {
    # -f: include filesystems
    lsblk -f
    # add columns
    lsblk -o +fstype,label,parttype
}

# === sfdisk ===
ptable() {
    case "$1" in
    b | backup) # copy/backup partition table (no filesystem info such as labels!)
        shift
        sfdisk -d "$1" >"${2:-ptable}.sfdisk"
        lsblk -pPo name,fstype,label "$1" >"${2:-ptable}.lsblk"
        ;;
    r | restore) # restore partition table
        shift
        sfdisk "$1" <"${2:-ptable}.sfdisk"
        local label part
        while read -r l; do
            label="$(awk '{print $3}' <<<"$l" | cut -d= -f2 | tr -d '"')"
            [[ -z "$label" ]] && continue
            part="$(awk '{print $1}' <<<"$l" | cut -d= -f2 | tr -d '"')"
            mkfs -L "\"${label}\"" "$part"
        done <"${2:-ptable}.lsblk"
        ;;
    *)
        echo "Usage: ${FUNCNAME[0]} backup(b)/restore(r) device [filename]"
        ;;
    esac
}

if [[ "$0" == "${BASH_SOURCE[0]}" ]]; then
    ptable "$@"
fi
