# while read -r l; do
#     label="$(awk '{print $3}' <<< "$l" | cut -d= -f2 | tr -d '"')"
#     [[ -z "$label" ]] && continue
#     part="$(awk '{print $1}' <<< "$l" | cut -d= -f2 | tr -d '"')"
#     echo mkfs -L "\"${label}\"" "$part"
# done < "${2:-ptable}.lsblk"

[[ "$1" =~ .jpg$|.jpeg$|.png$ ]] && echo "image provided" || echo "no image provided"
