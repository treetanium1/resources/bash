#!/usr/bin/env bash

. ../formatting.sh

echo "Test ${BASH_SOURCE[0]}"

for c in $F_COLORS; do
    [[ -n "${!c}" ]] && echo "${F_RESET}${!c}${c}${F_RESET}"
done

for c in $F_BCOLORS; do
    [[ -n "${!c}" ]] && echo "${F_RESET}${!c}${c}${F_RESET}"
done

for c in $F_UCOLORS; do
    [[ -n "${!c}" ]] && echo "${F_RESET}${!c}${c}${F_RESET}"
done
