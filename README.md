---
title: bash resources
desc-short: My bash resources
---

# bash resources

My stuff around bash. Scripts, snippets and more.


## External Scripts and Libraries


### getoptions

[getoptions @GitHub](https://github.com/ko1nksm/getoptions)

> An elegant option/argument parser for shell scripts (full support for bash and all POSIX shells)


## Testing

### ShellSpec

[ShellSpec website](https://shellspec.info/)