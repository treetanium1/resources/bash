#!/usr/bin/env bash

. ../getopts.sh

echo "Print help"
parse_options -h

parse_options "$@"
shift $((OPTIND - 1))

# test: print option values
$DRY && echo "Simulating.." || echo "Executing.."
$VERBOSE && echo "Verbose output.."

echo "All program arguments including options ($((OPTIND - 1))):"
echo "$@"
echo
# remove options from arguments (see man getopts: examples)
shift $((OPTIND - 1))
echo "Program arguments excluding options:"
echo "$@"

