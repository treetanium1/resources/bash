# Using tput seems to break my terminal (Konsole): using keys such as Home results in the displayed
# text being f'd up and the caret does weird shit
if [[ "$TERM" =~ 256 ]]; then
    # Formatting
    F_RESET="$(tput sgr0)"
    F_BOLD="$(tput bold)"
    F_UNDERLINE="$(tput smul)"
    # F_ITALIC="$(tput sitm)"

    # pre-defined types
    F_INFO="$(tput setaf 2)"
    F_ERROR="$(tput setaf 160)"
    F_WARN="$(tput setaf 214)"
    # Use like
    # echo "${info}INFO${Reset}: This is an ${bold}info${Reset} message"
    # echo "${error}ERROR${Reset}: This is an ${underline}error${Reset} message"
    # echo "${warn}WARN${Reset}: This is a ${italic}warning${Reset} message"

    # Colors
    F_BLACK="$(tput setaf 232)"
    F_GREY="$(tput setaf 250)"
    F_RED="$(tput setaf 1)"
    F_DARKRED="$(tput setaf 88)"
    F_GREEN="$(tput setaf 34)"
    F_YELLOW="$(tput setaf 226)"
    F_BLUE="$(tput setaf 69)"
    F_BRIGHTBLUE="$(tput setaf 80)"
    F_PURPLE="$(tput setaf 128)"
    F_CYAN="$(tput setaf 75)"
    F_WHITE="$(tput setaf 255)"
    F_TEAL="$(tput setaf 36)"
    F_ORANGE="$(tput setaf 214)"
    F_COLORS="F_BLACK F_GREY F_RED F_DARKRED F_GREEN F_YELLOW F_BLUE F_BRIGHTBLUE F_PURPLE F_CYAN F_WHITE F_TEAL F_ORANGE"

    # Bold
    for c in $F_COLORS; do
        eval "F_B${c#F_}=\$${c}\$F_BOLD"
    done
    F_BCOLORS="F_BBLACK F_BGREY F_BRED F_BDARKRED F_BGREEN F_BYELLOW F_BBLUE F_BBRIGHTBLUE F_BPURPLE F_BCYAN F_BWHITE F_BTEAL F_BORANGE"

    # Underlined
    for c in $F_COLORS; do
        eval "F_U${c#F_}=\$${c}\$F_UNDERLINE"
    done
    F_UCOLORS="F_UBLACK F_UGREY F_URED F_UDARKRED F_UGREEN F_UYELLOW F_UBLUE F_UBRIGHTBLUE F_UPURPLE F_UCYAN F_UWHITE F_UTEAL F_UORANGE"

# limited colour/formatting support, see e.g. http://colors.sh/
else
    # Formatting
    F_RESET="\e[0m"
    F_BOLD="\e[1m"
    F_ITALIC="\e[3m"
    F_UNDERLINE="\e[4m"

    # Regular Colors
    F_BLACK="\e[0;30m"
    F_RED="\e[0;31m"
    F_GREEN="\e[0;32m"
    F_YELLOW="\e[0;33m"
    F_ORANGE="\e[0;33m"
    F_BLUE="\e[0;34m"
    F_PURPLE="\e[0;35m"
    F_TEAL="\e[0;36m"
    F_GREY="\e[0;37m"

    # Bold
    F_BBLACK="\e[1;30m"
    F_BRED="\e[1;31m"
    F_BGREEN="\e[1;32m"
    F_BYELLOW="\e[1;33m"
    F_BORANGE="\e[1;33m"
    F_BBLUE="\e[1;34m"
    F_BPURPLE="\e[1;35m"
    F_BTEAL="\e[1;36m"
    F_BGREY="\e[1;37m"

    # Underlined
    F_UBLACK="\e[4;30m"
    F_URED="\e[4;31m"
    F_UGREEN="\e[4;32m"
    F_UYELLOW="\e[4;33m"
    F_UORANGE="\e[4;33m"
    F_UBLUE="\e[4;34m"
    F_UPURPLE="\e[4;35m"
    F_UTEAL="\e[4;36m"
    F_UGREY="\e[4;37m"
fi
